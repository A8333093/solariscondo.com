	<?php global $nz_ninzio;?>

		<!-- footer start -->
		<footer class='footer'>
			<?php get_sidebar('footer'); ?>
			<div class="footer-content">
				<div class="container nz-clearfix">
					<?php if ($nz_ninzio['footer-social-links'] && $nz_ninzio['footer-social-links'] == 1): ?>
						<div class="social-links">
							<?php get_template_part("/includes/social-links"); ?>
						</div>
					<?php endif ?>
					<?php if (has_nav_menu("footer-menu")): ?>
						<nav class="footer-menu nz-clearfix">
							<?php wp_nav_menu(array(
									'theme_location' => 'footer-menu',
									'depth'          => 1,
									'container'      => false,
									'menu_id'        => 'footer-menu',
							)); ?>
						</nav>
					<?php endif ?>
					<div class="footer-info">
	<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t38.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
//--></script><!--/LiveInternet-->
						<?php if ($nz_ninzio['footer-copyright']) { ?>
							<?php echo wp_kses_post($nz_ninzio['footer-copyright']); ?>
						<?php } ?>
					</div>
				
				</div>
			</div>
		</footer>
		<!-- footer end -->
	</div>
	<!-- wrap end -->

</div>
<!-- general wrap end -->
<?php if ($nz_ninzio['sidebar'] && $nz_ninzio['sidebar'] == 1){get_sidebar('sidebar');}; ?>
<a class="icon-arrow-up7" id="top" href="#wrap"></a>
<?php /*
<div id="custom_mail" style="
  position: fixed;
  bottom: 50px;
  right: 80px;
  z-index: 1;
">
	<div class="sidebar-toggle" title="Toggle sidebar" style="
	  background: url(/mail.png) rgba(0,0,0,.3);
	  background-size: 20px;
	  background-repeat: no-repeat;
	  background-position: center center;
	  width: 50px;
	  line-height: 50px;
	  text-align: center;
	  height: 50px;
	  cursor: pointer;
	  z-index: 49;
	  border-radius: 3px;
	  color: #ffffff;
	  -webkit-transition: all 300ms ease-out;
	  transition: all 300ms ease-out;
	">
	<style>
		.sidebar-toggle:hover {
			  background-color: #d4af37!important;
		}
	</style>
	</div>
</div>
*/ ?>
<?php if (isset($nz_ninzio['google-analytics']) && !empty($nz_ninzio['google-analytics'])) {
	echo '<script>'.$nz_ninzio['google-analytics'].'</script>';
} ?>
<?php include("includes/dynamic-scripts.php"); ?>
<?php wp_footer(); ?>
</body>
</html>